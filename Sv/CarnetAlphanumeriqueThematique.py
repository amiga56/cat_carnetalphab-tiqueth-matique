#coding: utf8
import configparser as cp
import sqlite3
import shutil,datetime
import sys, os, platform
from os.path import join
# import de la bibliothèque graphique
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GtkSource', '3.0')
from gi.repository import Gtk, Gdk, Pango, GtkSource

newdatafile= 'TEC Data.ini'
base='TEC Base.db'
state=("Defaut","A")
setupthemeselect=""
themes=[]
defaulttheme="Default"
defaultlettre="A"
button=[]
# TODO undo/redo
callback=False


def RepLancement():
    # en cas d'usage de PyInstaller pour créer un exe unique, il faut adapter le chemin du fichier ui,CSS
    if getattr(sys, 'frozen', False):
        wd = sys._MEIPASS
        print("bundle : path = ", wd,"plateforme :",platform.system())
        return wd
    else:
        wd = os.getcwd()
        print("live : path = ", wd,"plateforme :",platform.system())
        return wd

# Ouverture de la base de donnée ou création de celle-ci
try:
    with open(base):
        conn = sqlite3.connect(base)
        cursor = conn.cursor()
        # cursor.execute("""INSERT INTO element(theme, lettre, data) VALUES(?, ?, ?)""", ("Defaut","A","lettre A"))
        conn.commit()
        conn.close()
        pass
except IOError:
    print ("Erreur de base de données! Le fichier n'a pas pu être ouvert")
    # Création ou connection à la base de donnée
    conn = sqlite3.connect(base)
    # Création de la table
    cursor = conn.cursor()
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS element(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
     theme TEXT,lettre TEXT,data BLOB)
     """)
    conn.commit()
    conn.close()

# Ouverture du fichier de configuration ou création de celui-ci
try:
    with open(newdatafile):
        # Lecture de la configuration dans le .ini
        config = cp.ConfigParser()
        config.read(newdatafile)
        defaulttheme = config.get('DefaultTheme', 'Default')
        defaultlettre = config.get('DefaultLetter', 'Default')
        #print(config.sections())
        #print("Thème par défaut : ", defaulttheme)
        #print("Lettre par défaut : ", defaultlettre)
        for key in config['Themes']:
            #print("clé:", key, " - contenu :", config['Themes'][key])
            themes.append(config['Themes'][key])
        #print("Thèmes chargés :",themes)
        pass
except IOError:
    config = cp.ConfigParser()
    config.add_section('DefaultTheme')
    config.set('DefaultTheme', 'Default', 'Defaut')
    config.add_section('DefaultLetter')
    config.set('DefaultLetter','Default','A')
    defaulttheme='Default'
    config.add_section('Themes')
    config.set('Themes', 'Theme1', 'Defaut')
    config.set('Themes', 'Theme2', 'Python')
    themes.append('Defaut')
    themes.append('Python')
    with open(newdatafile, 'w') as settings:
        config.write(settings)
    print(themes)

def printlog(self,txt):
    self.log.insert_at_cursor(txt+"\n")

def trouveElementBase(self, theme,lettre):
    self.buffer.delete(self.buffer.get_start_iter(), self.buffer.get_end_iter())
    conn = sqlite3.connect(base)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM element WHERE theme ='" + theme + "' AND lettre ='" + lettre + "'")
    try:
        resultat = list(cursor)[0][3]
        format = self.buffer.register_deserialize_tagset()
        data = self.buffer.deserialize(self.buffer, format, self.buffer.get_end_iter(), resultat)
    except IndexError:
        printlog(self,"Pas encore d'entrée, création.")
        resultat = ""
        # TODO : Pour les nouveaux éléments taile text par defaut
        self.buffer.delete(self.buffer.get_start_iter(), self.buffer.get_end_iter())
        self.buffer.insert_at_cursor("Thème : "+theme+ " / Lettre "+lettre)
        self.buffer.apply_tag(self.tag_size12, self.buffer.get_start_iter(), self.buffer.get_end_iter())
    conn.close()
    printlog(self,"Lecture du thème {0}, lettre {1}".format(theme, lettre))
    self.label.set_text("Edition de la lettre "+lettre+" du thème "+theme+".")
    # Decoration du bouton
    for i,l in enumerate("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"):
        if lettre == l:
            print("déco du bouton",i)
            button[i].get_style_context().add_class("suggested-action")

def sauveElementBase(self, theme,lettre):
    conn = sqlite3.connect(base)
    cursor = conn.cursor()

    # Sérialisation des données du buffer de TextView afin de les places dans la base SQL
    data = self.buffer.get_text(self.buffer.get_start_iter(), self.buffer.get_end_iter(), include_hidden_chars=True)
    print("Sauvegarde du thème {0}, lettre {1}".format(theme, lettre))
    # print("Sauvegarde du thème {0}, lettre {1}, Data : {2}".format(theme, lettre, data[:40]))
    format = self.buffer.register_serialize_tagset()
    # print("Format : ",format)
    data = self.buffer.serialize(self.buffer, format, self.buffer.get_start_iter(), self.buffer.get_end_iter())
    cursor.execute("SELECT * FROM element WHERE theme ='" + theme + "' AND lettre ='" + lettre + "'")
    try:
        resultat = list(cursor)[0][3]
        #print(resultat)
        #cursor.execute("UPDATE element SET data='"+data+"' WHERE theme='"+theme+"' AND lettre='"+lettre+"'")
        cursor.execute("""UPDATE element SET data = ? WHERE theme = ? AND lettre = ?""", (data,theme,lettre))
    except IndexError:
        print("Erreur les données n'existent pas, création ...")
        cursor.execute("""INSERT INTO element(theme, lettre, data) VALUES(?, ?, ?)""", (theme, lettre, data))
        resultat = ""
    conn.commit()
    conn.close()
    return resultat

# Enlève la déco des tous les boutons lorsque l'on change de lettre
def removeDeco(self):
    for i in range(36):
        button[i].get_style_context().remove_class("suggested-action")

# Les trois fonctions suivantes enlèvent les tags du GtkTextView.
def removeColorTags(self,start,end):
    self.buffer.remove_tag(self.tag_black, start, end)
    self.buffer.remove_tag(self.tag_blue, start, end)
    self.buffer.remove_tag(self.tag_red, start, end)
    self.buffer.remove_tag(self.tag_green, start, end)
    self.buffer.remove_tag(self.tag_yellow, start, end)

def removeSizeTag(self,start,end):
    self.buffer.remove_tag(self.tag_size, start, end)
    self.buffer.remove_tag(self.tag_size10, start, end)
    self.buffer.remove_tag(self.tag_size11, start, end)
    self.buffer.remove_tag(self.tag_size12, start, end)
    self.buffer.remove_tag(self.tag_size13, start, end)
    self.buffer.remove_tag(self.tag_size14, start, end)
    self.buffer.remove_tag(self.tag_size15, start, end)

def removeTags(self,start,end):
    removeSizeTag(self,start,end)
    self.buffer.remove_tag(self.tag_bold, start, end)
    self.buffer.remove_tag(self.tag_italic, start, end)
    self.buffer.remove_tag(self.tag_underline, start, end)
    self.buffer.remove_tag(self.tag_center, start, end)
    self.buffer.remove_tag(self.tag_left, start, end)
    self.buffer.remove_tag(self.tag_right, start, end)
    self.buffer.remove_tag(self.tag_titre, start, end)
    self.buffer.remove_tag(self.tag_stitre1, start, end)
    self.buffer.remove_tag(self.tag_stitre2, start, end)
    self.buffer.remove_tag(self.tag_strikethrough, start, end)

def removeAlignTags(self,start,end):
    self.buffer.remove_tag(self.tag_center, start, end)
    self.buffer.remove_tag(self.tag_left, start, end)
    self.buffer.remove_tag(self.tag_right, start, end)

# Fonction pour les boutons A à Z et 0 à 9 - l'appel vient des callback des boutons
def ActionAlphaButtons(self,lettre):
    global defaulttheme, defaultlettre
    print("Bouton '"+lettre+"'")
    # Mise a jour de la déco des boutons
    removeDeco(self)
    for i, l in enumerate("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"):
        if lettre == l:
            button[i].get_style_context().add_class("suggested-action")
    # Sauve l'élément courant et désigne le nouvel élément avant de le charger
    sauveElementBase(self, defaulttheme, defaultlettre)
    defaultlettre = lettre
    trouveElementBase(self, defaulttheme, defaultlettre)
#####################################
# Définition de la classe 'Interface'
#####################################
class Interface:
    __gtype_name__ = "CarnetAlphabétiqueThèmatique"

    # initialisateur de la méthode 'Interface' (état de départ)
    def __init__(self):
        global wd
        wd = RepLancement()
        # Création et chargement de l'interface
        interface = Gtk.Builder()
        interface.add_from_file('Interface.glade')

        # Connecter les signaux
        interface.connect_signals(self)
        # variables

        self.textbuffer = GtkSource.Buffer()
        self.box1=interface.get_object('box1')
        textview = GtkSource.View(buffer=self.textbuffer)
        textview.set_editable(True)
        textview.set_cursor_visible(True)
        textview.set_show_line_numbers(True)
        self.box1.pack_start(textview, True, True, 2)

        self.window = interface.get_object("window")
        self.set_style()
        self.setup = interface.get_object("Dialog1")
        self.label = interface.get_object('labelremarque')

        # Définition des boutons A......0...9
        for i in "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789":
            button.append(interface.get_object('button'+i))

        # Objets de la fenêtre principale
        self.textviewlog = interface.get_object('textviewlog')
        self.scrolledwindow = interface.get_object('scrolledwindow')
        self.log = self.textviewlog.get_buffer()
        self.textview=interface.get_object('textview')
        self.buffer = self.textview.get_buffer()
        self.buffer.connect("changed",self.selection)

        self.tag_bold = self.buffer.create_tag("bold", weight=Pango.Weight.BOLD)
        self.tag_italic = self.buffer.create_tag("italic", style=Pango.Style.ITALIC)
        self.tag_underline = self.buffer.create_tag("underline", underline=Pango.Underline.SINGLE)
        self.tag_strikethrough = self.buffer.create_tag("strikethrough", strikethrough=True)
        self.tag_center = self.buffer.create_tag("centre", justification=Gtk.Justification.CENTER)
        self.tag_left = self.buffer.create_tag("left", justification=Gtk.Justification.LEFT)
        self.tag_right = self.buffer.create_tag("right", justification=Gtk.Justification.RIGHT)
        self.tag_found = self.buffer.create_tag("found", background="yellow")
        self.tag_size = self.buffer.create_tag("size", size_points=10)
        self.tag_size10 = self.buffer.create_tag("size10", size_points=10)
        self.tag_size11 = self.buffer.create_tag("size11", size_points=11)
        self.tag_size12 = self.buffer.create_tag("size12", size_points=12)
        self.tag_size13 = self.buffer.create_tag("size13", size_points=13)
        self.tag_size14 = self.buffer.create_tag("size14", size_points=14)
        self.tag_size15 = self.buffer.create_tag("size15", size_points=15)
        self.tag_titre = self.buffer.create_tag("titre", weight=Pango.Weight.BOLD, underline=Pango.Underline.SINGLE, size_points=15)
        self.tag_stitre1 = self.buffer.create_tag("stitre1", weight=Pango.Weight.BOLD, underline=Pango.Underline.SINGLE, size_points=12)
        self.tag_stitre2 = self.buffer.create_tag("stitre2", weight=Pango.Weight.BOLD, size_points=12)

        self.tag_black = self.buffer.create_tag("black", foreground="black")
        self.tag_blue = self.buffer.create_tag("blue", foreground="blue")
        self.tag_red = self.buffer.create_tag("red", foreground="red")
        self.tag_green = self.buffer.create_tag("green", foreground="green")
        self.tag_yellow = self.buffer.create_tag("yellow", foreground="yellow")

        printlog(self, "Backup de la base de données")
        date = str(datetime.datetime.now())
        date = date.replace(":", "-")
        shutil.copy("TEC Base.db", "TEC Base " + str(date) + ".db")
        # print ("init : DT {0} - DL {1}".format(defaulttheme,defaultlettre))
        # Chargement initial de l'élément pas défaut
        # print("Intreface, Thème par défaut : ", defaulttheme)
        #print("Intreface, Lettre par défaut : ", defaultlettre)
        removeDeco(self)
        trouveElementBase(self, defaulttheme, defaultlettre)

        # combo de la fenêtre principale
        self.combostyle = interface.get_object('comboboxtextstyle')
        self.combo= interface.get_object('comboboxtexttheme')
        self.combo.remove_all()
        for i,elt in enumerate(themes):
            self.combo.append_text(elt)
            if elt==defaulttheme:
                self.combo.set_active(i)
        print("init : DT {0} - DL {1}".format(defaulttheme, defaultlettre))
        # Objets du dialogue
        self.nom = interface.get_object('entryDialogNom')
        # Objets du dialogue : TreeView
        self.treeview = interface.get_object('treeview')
        renderer=Gtk.CellRendererText()
        column=Gtk.TreeViewColumn("Liste des thèmes",renderer,text=0)
        self.treeview.append_column(column)
        self.list_store1= Gtk.ListStore(str)
        self.list_store1.append(["test1"])
        self.list_store1.append(["test2"])
        self.treeview.set_model(self.list_store1)

        # montrer l'interface
        self.window.show_all()

##############################################################################
    def set_style(self):
        """ Change Gtk+ Style """
        provider = Gtk.CssProvider()
        # Demo CSS kindly provided by Numix project
        provider.load_from_path(join(wd, 'MyCSS.css'))
        screen = Gdk.Display.get_default_screen(Gdk.Display.get_default())
        # I was unable to found instrospected version of this
        GTK_STYLE_PROVIDER_PRIORITY_APPLICATION = 600
        Gtk.StyleContext.add_provider_for_screen(
            screen, provider,
            GTK_STYLE_PROVIDER_PRIORITY_APPLICATION
        )
    # Astuce permet le defilement automatique du treeviw ds la scrolledwindow
    # GtkTextview - Signals - GtkWidget - size-allocate
    def treeview_changed(self, widget, event, data=None):
        adj = self.scrolledwindow.get_vadjustment()
        adj.set_value(adj.get_upper() - adj.get_page_size())

    def onButtonSetup(self,widget):
        self.setup.show_all()
        # Effacer le GtkTreeView (list_store) & chargement de la liste des thèmes
        self.list_store1.clear()
        log="Liste des thèmes : "
        for line in themes:
            log=log+line+" - "
            self.list_store1.append([line])
        printlog(self, log[:len(log)-3])
        reponse = self.setup.run()
        print(reponse)
        if reponse == 0: self.setup.hide() # Sortie avec Quit
        if reponse == -4: self.setup.hide()  # Sortie avec la croix
        if reponse == 1: # Sortie avec ok
            self.setup.hide()
            print("Enregistrement des thèmes")
            config = cp.ConfigParser()
            config.read(newdatafile)
            # On efface la liste des themes pour la recréer
            config.remove_section('Themes')
            config.add_section('Themes')
            for i,theme in enumerate(themes):
                config.set('Themes', 'Theme'+str(i), theme)
            # Mise à jour du .ini
            with open(newdatafile, 'w') as settings:
                config.write(settings)
            # Mise a jour du combobox
            self.combo.remove_all()
            log = "Liste des thèmes : "
            for i, elt in enumerate(themes):
                log = log + elt + " - "
                self.combo.append_text(elt)
                if elt == defaulttheme:
                    self.combo.set_active(i)
            printlog(self, log[:len(log) - 3])

    def OnButtonDialogNettoyer(self, widget):
        printlog(self, "Nettoyage base.")
        global themes
        conn = sqlite3.connect(base)
        cursor = conn.cursor()
        sql='SELECT * FROM element WHERE theme NOT IN ({})'.format(','.join('?' * len(themes)))
        row= cursor.execute(sql, themes).fetchone()
        # Recherche de la liste des thèmes à effacer
        deletedThemes=[]
        while row != None:
            rep=0
            for testth in deletedThemes:
                if row[1] == testth:
                    rep=1 # Déjà dans la liste
            if rep==0:
                # Le thème à effacer n'est pas déjà dans la liste, on l'ajoute
                deletedThemes.append(row[1])
            row = cursor.fetchone() # Entrée suivante qui contient des données à effacer
        print("Thèmes à détruire : ",deletedThemes)
        if deletedThemes !="":
            for th in deletedThemes:
                printlog(self,"Effacement des entrées du thème : {0}".format(th))
                cursor.execute("DELETE FROM element WHERE theme = '"+th+"'")
        conn.commit()
        conn.close()

    def OnButtonDialogSup(self, widget):
        global setupthemeselect
        print("dialog,sup")
        # Le thème sélectionné ne doit pas être celui actif
        if setupthemeselect == defaulttheme:
            dialog1 = Gtk.MessageDialog(parent=Gtk.Window(), flags=0, message_type=Gtk.MessageType.INFO,
                                        buttons=Gtk.ButtonsType.OK, text="Le thème à supprimer est celui actif !")
            dialog1.format_secondary_text("Activer un autre thème pour supprimer !.")
            dialog1.run()
            dialog1.destroy()
        else:
            if setupthemeselect == "":
                dialog1 = Gtk.MessageDialog(parent=Gtk.Window(), flags=0, message_type=Gtk.MessageType.INFO,
                                            buttons=Gtk.ButtonsType.OK, text="pas de thème sélectionné !")
                dialog1.run()
                dialog1.destroy()
            for i,suptheme in enumerate(themes):
                if suptheme == setupthemeselect:
                    printlog(self,"thème à supprimer trouvé dans la liste")
                    del themes[i]
            # Effacer le GtkTreeView (list_store) et le remettre à jour
            self.list_store1.clear()
            for line in themes:
                self.list_store1.append([line])


    def onTreeSelectionChanged(self, selection):
        global setupthemeselect
        model, treeiter = selection.get_selected()
        if treeiter is not None:
            print("You selected", model[treeiter][0])
            setupthemeselect=model[treeiter][0]

    def OnButtonDialogCreer(self, widget):
        print("dialog, créer")
        nom = self.nom.get_text()
        # S'assurer que le nom n'existe pas deja
        error=False
        for enom in themes:
            if nom == enom:
                dialog1 = Gtk.MessageDialog(parent=Gtk.Window(), flags=0, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK, text="Le thème existe déjà !")
                dialog1.format_secondary_text("Changer de nom !.")
                dialog1.run()
                dialog1.destroy()
                error=True
        if nom != "" and error is False:
            themes.append(nom)
            self.list_store1.append([nom])
        print(themes)

    # lien avec glade : comboboxtexttheme / signaux / GtkComboBox / changed -> comboboxtexttheme_changed
    def comboboxtexttheme_changed(self, widget, data=None):
        global defaulttheme, defaultlettre,callback
        model = widget.get_model()
        active = widget.get_active()
        print("ComboThème")
        if active >= 0:
            if  callback == True:
                temptheme = model[active][0]
                print("Thème : ", temptheme, "n°", active)
                # Sauvegarde de l'élément courant
                sauveElementBase(self,defaulttheme, defaultlettre)
                # Changement du thème par defaut
                defaulttheme=temptheme
                # Chargement de l'élément, lettre "A" imposé
                # defaultlettre = "A"
                trouveElementBase(self, defaulttheme, defaultlettre)
            else:
                callback = True
        else:
            print("erreur combo")

    def comboboxstyle_changed(self,widget,data=None):
        model = widget.get_model()
        active = widget.get_active()
        if active >= 0:
            style = model[active][0]
            print("Ch taille :", style)
            bounds = self.buffer.get_selection_bounds()
            if len(bounds) != 0:
                start, end = bounds
                if style =="Standart":
                    self.buffer.remove_all_tags(start, end)
                if style =="Titre":
                    removeTags(self, start, end)
                    self.buffer.apply_tag(self.tag_titre, start, end)
                if style == "Sous Titre 1":
                    removeTags(self, start, end)
                    self.buffer.apply_tag(self.tag_stitre1, start, end)
                if style == "Sous Titre 2":
                    removeTags(self, start, end)
                    self.buffer.apply_tag(self.tag_stitre2, start, end)

    def selection(self,widget,data=None):
        # print("Sélection de texte")
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            # print("start :",start.begins_tag(self.tag_titre),"-",start.ends_tag(self.tag_titre),"-",start.toggles_tag(self.tag_titre),"-",start.has_tag(self.tag_titre))
            # print("end :", end.begins_tag(self.tag_titre), "-", end.ends_tag(self.tag_titre), "-", end.toggles_tag(self.tag_titre), "-", end.has_tag(self.tag_titre))
            print(start.has_tag(self.tag_titre),"-",end.has_tag(self.tag_titre))
            if start.toggles_tag(self.tag_titre) and end.toggles_tag(self.tag_titre):
                print("vrai titre")
                self.combostyle.set_active(0)
            if start.toggles_tag(self.tag_stitre1) and end.toggles_tag(self.tag_stitre1):
                print("vrai sous titre 1")
                self.combostyle.set_active(1)
            if start.toggles_tag(self.tag_stitre2) and end.toggles_tag(self.tag_stitre2):
                print("vrai sous titre 2")
                self.combostyle.set_active(2)
            if not start.toggles_tag(self.tag_titre) and not end.toggles_tag(self.tag_titre) and not start.toggles_tag(self.tag_stitre1) and not end.toggles_tag(self.tag_stitre1) and not start.toggles_tag(self.tag_stitre2) and not end.toggles_tag(self.tag_stitre2):
                print("vrai aucun style")
                self.combostyle.set_active(3)

    def comboboxtextpoints_changed(self,widget,data=None):
        model = widget.get_model()
        active = widget.get_active()
        if active >= 0:
            size = model[active][0]
            print("Ch taille :",size)
            bounds = self.buffer.get_selection_bounds()
            if len(bounds) != 0:
                start, end = bounds
                if size == "10":
                    removeSizeTag(self,start,end)
                    self.buffer.apply_tag(self.tag_size10, start, end)
                if size == "11":
                    removeSizeTag(self,start,end)
                    self.buffer.apply_tag(self.tag_size11, start, end)
                if size == "12":
                    removeSizeTag(self, start, end)
                    self.buffer.apply_tag(self.tag_size12, start, end)
                if size == "13":
                    removeSizeTag(self, start, end)
                    self.buffer.apply_tag(self.tag_size13, start, end)
                if size == "14":
                    removeSizeTag(self, start, end)
                    self.buffer.apply_tag(self.tag_size14, start, end)
                if size == "15":
                    removeSizeTag(self, start, end)
                    self.buffer.apply_tag(self.tag_size15, start, end)

    def on_button_blue_clicked(self,widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeColorTags(self,start,end)
            self.buffer.apply_tag(self.tag_blue, start, end)

    def on_button_red_clicked(self,widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeColorTags(self,start,end)
            self.buffer.apply_tag(self.tag_red, start, end)

    def on_button_green_clicked(self,widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeColorTags(self,start,end)
            self.buffer.apply_tag(self.tag_green, start, end)

    def on_button_yellow_clicked(self,widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeColorTags(self,start,end)
            self.buffer.apply_tag(self.tag_yellow, start, end)

    def on_button_black_clicked(self,widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeColorTags(self,start,end)
            self.buffer.apply_tag(self.tag_black, start, end)

    def on_button_bold_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            self.buffer.apply_tag(self.tag_bold,start,end)

    def on_button_italic_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            self.buffer.apply_tag(self.tag_italic,start,end)

    def on_button_underline_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            self.buffer.apply_tag(self.tag_underline,start,end)

    def on_button_strikethrough_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            self.buffer.apply_tag(self.tag_strikethrough,start,end)

    def on_button_gauche_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeAlignTags(self,start,end)
            self.buffer.apply_tag(self.tag_left,start,end)

    def on_button_droite_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeAlignTags(self,start,end)
            self.buffer.apply_tag(self.tag_right,start,end)

    def on_button_centre_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeAlignTags(self,start,end)
            self.buffer.apply_tag(self.tag_center,start,end)

    # Enlever tous les tags d'une zone.
    def on_button_effacer_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeTags(self, start, end)
            removeAlignTags(self, start, end)
            removeColorTags(self, start, end)

    # Liste de tous les callback des boutons A à Z et 0 à 9 - Chaque callback appel ActionAlphaButton()
    # lien avec glade pour tous les boutons : Gtkbutton / Signals / GtkButton / clicked ->onButton...Clicked
    def onButtonAClicked(self, widget):
        ActionAlphaButtons(self,"A")
    def onButtonBClicked(self, widget):
        ActionAlphaButtons(self,"B")
    def onButtonCClicked(self, widget):
        ActionAlphaButtons(self,"C")
    def onButtonDClicked(self, widget):
        ActionAlphaButtons(self,"D")
    def onButtonEClicked(self, widget):
        ActionAlphaButtons(self,"E")
    def onButtonFClicked(self, widget):
        ActionAlphaButtons(self,"F")
    def onButtonGClicked(self, widget):
        ActionAlphaButtons(self,"G")
    def onButtonHClicked(self, widget):
        ActionAlphaButtons(self,"H")
    def onButtonIClicked(self, widget):
        ActionAlphaButtons(self,"I")
    def onButtonJClicked(self, widget):
        ActionAlphaButtons(self,"J")
    def onButtonKClicked(self, widget):
        ActionAlphaButtons(self,"K")
    def onButtonLClicked(self, widget):
        ActionAlphaButtons(self,"L")
    def onButtonMClicked(self, widget):
        ActionAlphaButtons(self,"M")
    def onButtonNClicked(self, widget):
        ActionAlphaButtons(self,"N")
    def onButtonOClicked(self, widget):
        ActionAlphaButtons(self,"O")
    def onButtonPClicked(self, widget):
        ActionAlphaButtons(self,"P")
    def onButtonQClicked(self, widget):
        ActionAlphaButtons(self,"Q")
    def onButtonRClicked(self, widget):
        ActionAlphaButtons(self,"R")
    def onButtonSClicked(self, widget):
        ActionAlphaButtons(self,"S")
    def onButtonTClicked(self, widget):
        ActionAlphaButtons(self,"T")
    def onButtonUClicked(self, widget):
        ActionAlphaButtons(self,"U")
    def onButtonVClicked(self, widget):
        ActionAlphaButtons(self,"V")
    def onButtonWClicked(self, widget):
        ActionAlphaButtons(self,"W")
    def onButtonXClicked(self, widget):
        ActionAlphaButtons(self,"X")
    def onButtonYClicked(self, widget):
        ActionAlphaButtons(self,"y")
    def onButtonZClicked(self, widget):
        ActionAlphaButtons(self,"Z")
    def onButton0Clicked(self, widget):
        ActionAlphaButtons(self,"0")
    def onButton1Clicked(self, widget):
        ActionAlphaButtons(self,"1")
    def onButton2Clicked(self, widget):
        ActionAlphaButtons(self,"2")
    def onButton3Clicked(self, widget):
        ActionAlphaButtons(self,"3")
    def onButton4Clicked(self, widget):
        ActionAlphaButtons(self,"4")
    def onButton5Clicked(self, widget):
        ActionAlphaButtons(self,"5")
    def onButton6Clicked(self, widget):
        ActionAlphaButtons(self,"6")
    def onButton7Clicked(self, widget):
        ActionAlphaButtons(self,"7")
    def onButton8Clicked(self, widget):
        ActionAlphaButtons(self,"8")
    def onButton9Clicked(self, widget):
        ActionAlphaButtons(self,"9")

    # Lien avec glade : windows / Signals / GtkWidget / destroy -> on_destroy
    def on_destroy(self, widget):
        # Sauve l'élément courant dans la base
        print("Appel sauvegarde fin")
        sauveElementBase(self, defaulttheme, defaultlettre)
        # Sauve la configuration
        config = cp.ConfigParser()
        config.read(newdatafile)
        # Modification des thèmes et lettre par défaut
        config.set('DefaultTheme', 'Default', defaulttheme)
        config.set('DefaultLetter', 'Default', defaultlettre)
        # Mise à jour du .ini
        with open(newdatafile, 'w') as settings:
            config.write(settings)
        print("Au Revoir !")
        Gtk.main_quit()

# exécute le module sauf s'il est importé
if __name__ == "__main__":
    # Création de l'interface graphique
    app = Interface()
    # Boucle principale
    Gtk.main()