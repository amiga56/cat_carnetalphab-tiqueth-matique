# la doc de PyInstaller ici : https://pythonhosted.org/PyInstaller/index.html et ici : https://pyinstaller.readthedocs.io/en/stable/


E:\msys32\mingw64\bin\python3.exe -m PyInstaller --add-data "*.glade;." --add-data "*.db;." --add-data "*.ini;." --add-data "*.css;." --workpath "L:\Travail\Temp\Build" --distpath "L:\Travail\Temp\Dist" --windowed  -n CarnetAlphabetiqueThematique CarnetAlphabetiqueThematique.py
:: E:\msys32\mingw64\bin\python3.exe -m PyInstaller --add-data "*.glade;." --add-data "*.css;." --workpath "L:\Travail\Temp\Build" --distpath "L:\Travail\Temp\Dist"  -n CarnetAlphabétiqueThèmatique --onefile CarnetAlphabetiqueThematique.py
pause

le --add-data permet d'ajouter des fichier au bundle

avec patch du code python pour tenir compte du chemin des fichier en mode "fozen, bundles" :

voir ici : https://stackoverflow.com/questions/14296833/pyinstaller-onefile-doesnt-find-data-files
et ici : https://pythonhosted.org/PyInstaller/runtime-information.html#using-file-and-sys-meipass

		if getattr(sys, 'frozen', False):
           print("bundle")
           wd = sys._MEIPASS
        else:
           print("live")
           wd = os.getcwd()
        file_path = os.path.join(wd,'Calcul_De_Vis.glade')

ou

python -m PyInstaller --add-data "Calcul_De_Vis.glade;." --onfile Calcul_De_vis.py