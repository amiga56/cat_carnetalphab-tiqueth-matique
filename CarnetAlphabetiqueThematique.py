#coding: utf-8
import configparser as cp
import sqlite3
import shutil,datetime
import sys, os, platform
from os.path import join
# import de la bibliothèque graphique
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GtkSource', '3.0')
from gi.repository import Gtk, Gdk, Pango,GtkSource
from PyQt5 import QtGui,QtCore

# A FAIRE: ez
# TODO: Imprimer
# TODO: mise a jour de la taille de texte en fonction de la position du curseur

newdatafile= 'TEC Data.ini'
base = 'TEC Base.db'
ini = 'TEC Data.ini'
state=("Defaut","A")
setupthemeselect=""
themes=[]
defaulttheme="Defaut"
defaultlettre="A"
defaultdir=""
button=[]
callback=False
togglebuttonstate=False
initpypandoc=False
tags=[]
debug = False

def SaveFileInOdt(data,file):
    tag=""
    tags=[]
    tags.append("normal") # au moins un tag

    # Create a document object
    doc = QtGui.QTextDocument()
    # Create a cursor pointing to the beginning of the document
    cursor = QtGui.QTextCursor(doc)
    # Définition du format pour les textes du QText
    current_format=QtGui.QTextCharFormat()

    def setTitre(state):
        if debug: print("<<<<<<<<< tag Titre (", state, ") >>>>>>>")
        if state:
            current_format.setBackground(QtCore.Qt.gray)
            setBold(True)
            setUnderline(True)
            setSizePoint(18)
        else:
            current_format.setBackground(QtCore.Qt.white)
            setBold(False)
            setUnderline(False)
            setSizePoint(12)

    def setSTitre1(state):
        if debug: print("<<<<<<<<< tag Stitre1 (", state, ") >>>>>>>")
        if state:
            current_format.setBackground(QtCore.Qt.lightGray)
            setBold(False)
            setUnderline(True)
            setSizePoint(15)
        else:
            current_format.setBackground(QtCore.Qt.white)
            setBold(False)
            setUnderline(False)
            setSizePoint(12)

    def setSTitre2(state):
        if debug: print("<<<<<<<<< tag STitre2 (", state, ") >>>>>>>")
        if state:
            setBold(True)
            setUnderline(False)
            setSizePoint(15)
        else:
            setBold(False)
            setUnderline(False)
            setSizePoint(12)

    def setSizePoint(size):
        if debug: print("<<<<<<<<< tag size (", size, ") >>>>>>>")
        current_format.setFontPointSize(size)

    def setBold(state):
        if debug: print("<<<<<<<<< tag Bold (", state, ") >>>>>>>")
        if state:
            current_format.setFontWeight(QtGui.QFont.Bold)
        else:
            current_format.setFontWeight(QtGui.QFont.Normal)

    def setItalic(state):
        if debug: print("<<<<<<<<< tag Italic (", state, ") >>>>>>>")
        if state:
            current_format.setFontItalic(True)
        else:
            current_format.setFontItalic(False)

    def setStrike(state):
        if debug: print("<<<<<<<<< tag strike (", state, ") >>>>>>>")
        current_format.setFontStrikeOut(state)

    def setUnderline(state):
        if debug: print("<<<<<<<<< tag underline (", state, ") >>>>>>>")
        if state:
            current_format.setUnderlineStyle(QtGui.QTextCharFormat.SingleUnderline)
        else:
            current_format.setUnderlineStyle(QtGui.QTextCharFormat.NoUnderline)

    def setRed(state):
        if debug: print("<<<<<<<<< tag red (", state, ") >>>>>>>")
        if state:
            current_format.setForeground(QtCore.Qt.red)
        else:
            current_format.setForeground(QtCore.Qt.black)

    def setBlue(state):
        if debug: print("<<<<<<<<< tag blue (",state,") >>>>>>>")
        if state:
            current_format.setForeground(QtCore.Qt.blue)
        else:
            current_format.setForeground(QtCore.Qt.black)

    def setGreen(state):
        if debug: print("<<<<<<<<< tag blue (",state,") >>>>>>>")
        if state:
            current_format.setForeground(QtCore.Qt.green)
        else:
            current_format.setForeground(QtCore.Qt.black)

    def setYellow(state):
        if debug: print("<<<<<<<<< tag blue (",state,") >>>>>>>")
        if state:
            current_format.setForeground(QtCore.Qt.yellow)
        else:
            current_format.setForeground(QtCore.Qt.black)

    def setNormal():
        if debug: print("<<<<<<<<< tag Normal >>>>>>>")
        setUnderline(False)
        setItalic(False)
        setBold(False)
        setStrike(False)
        setSizePoint(12)

    # Alignment
    center_format = QtGui.QTextBlockFormat()
    center_format.setAlignment(QtCore.Qt.AlignCenter)
    right_format = QtGui.QTextBlockFormat()
    right_format.setAlignment(QtCore.Qt.AlignRight)
    left_format = QtGui.QTextBlockFormat()
    left_format.setAlignment(QtCore.Qt.AlignLeft)

    def setTags():
        if len(tags) != 0:
            if tags[-1] == "titre":
                setTitre(True)
            if tags[-1] == "stitre1":
                setSTitre1(True)
            if tags[-1] == "stitre2":
                setSTitre2(True)
            if tags[-1] == "centre":
                cursor.setBlockFormat(center_format)
            if tags[-1] == "right":
                cursor.setBlockFormat(right_format)
            if tags[-1] == "normal":
                setNormal()
            if tags[-1] == "italic":
                setItalic(True)
            if tags[-1] == "bold":
                setBold(True)
            if tags[-1] == "underline":
                setUnderline(True)
            if tags[-1] == "strikethrough":
                setStrike(True)
            if tags[-1] == "red":
                setRed(True)
            if tags[-1] == "blue":
                setBlue(True)
            if tags[-1] == "green":
                setGreen(True)
            if tags[-1] == "yellow":
                setYellow(True)
            if tags[-1] == "size10":
                setSizePoint(10)
            if tags[-1] == "size11":
                setSizePoint(11)
            if tags[-1] == "size12":
                setSizePoint(12)
            if tags[-1] == "size13":
                setSizePoint(13)
            if tags[-1] == "size14":
                setSizePoint(14)
            if tags[-1] == "size15":
                setSizePoint(15)
        else:
            setNormal()

    def setUnTags():
        if tags[-1] == "titre":
            setTitre(False)
        if tags[-1] == "stitre1":
            setSTitre1(False)
        if tags[-1] == "stitre2":
            setSTitre2(False)
        if tags[-1] == "centre":
            cursor.insertBlock(left_format)
        if tags[-1] == "right":
            cursor.insertBlock(left_format)
        if tags[-1] == "underline":
            setUnderline(False)
        if tags[-1] == "strikethrough":
            setStrike(False)
        if tags[-1] == "italic":
            setItalic(False)
        if tags[-1] == "bold":
            setBold(False)
        if tags[-1] == "red":
            setRed(False)
        if tags[-1] == "blue":
            setBlue(False)
        if tags[-1] == "green":
            setGreen(False)
        if tags[-1] == "yellow":
            setYellow(False)
        if tags[-1] == "size10":
            setSizePoint(12)
        if tags[-1] == "size11":
            setSizePoint(12)
        if tags[-1] == "size13":
            setSizePoint(12)
        if tags[-1] == "size14":
            setSizePoint(12)
        if tags[-1] == "size15":
            setSizePoint(12)

    #########################################
    # Préparation des données à sauvegarder #
    #########################################
    # pprint(data)
    if debug: print("data", data)
    # Enlever les '<br>' éventuels en début et en fin
    if data.find('<br>') == 0:
        # br au début
        data = data[4:]
    if data[-1] == ">" and data[-2] == "r" and data[-3] == "b" and data[-4] == "<":
        # br a la fin
        data = data[:len(data) - 4]
    # Enlever les balises '<text>" el les '<br>' en double
    data = data.replace("<text>", "")
    data = data.replace("</text>", "")
    data = data.replace("<br><br><br><br><br><br>", "<br>")
    data = data.replace("<br><br><br><br><br>", "<br>")
    data = data.replace("<br><br><br><br>", "<br>")
    data = data.replace("<br><br><br>", "<br>")
    data = data.replace("<br><br>", "<br>")
    # Enlever les '<br>' éventuels en début et en fin aprés suppression des balises '<text>'
    if data.find('<br>') == 0:
        # br au début
        data = data[4:]
    if data[-1] == ">" and data[-2] == "r" and data[-3] == "b" and data[-4] == "<":
        # br a la fin
        data = data[:len(data) - 4]
    # Nettoyage des balises inutiles et génantes
    data = data.replace('<apply_tag name="size12"><br><br></apply_tag>', "<br>")  # new
    data = data.replace('<apply_tag name="size12"><br></apply_tag>', "<br>")  # new
    data = data.replace('<apply_tag name="size12"></apply_tag>', "")  # new
    if debug: print("Data après filtrage : ", data,)
    i = 0
    depart = True
    while depart:
        i += 1
        # Empiler le premier tag
        if debug: print("Initial : ", data)
        t0 = data.find("<")
        t1 = data.find(">")
        t2 = data.find("/")
        if debug: print("< en position :", t0, "- > en position :", t1, "- / en position :", t2)
        # print(element[t0:t1+1])
        if data.find('<br>') == 0:
            if debug: print("l'élément suivant est un '<br>' ")
            cursor.insertText("\n")
            data = data[4:]
        else:
            if t0 == 0 and t2 > t1:
                if debug: print("l'élément suivant est un tag : ", data[t0:t1 + 1], "- Tous les tags :", tags)
                # Quel tag ?
                tagbrut = data[t0:t1 + 1]
                tb0 = tagbrut.find('=')
                tb1 = tagbrut.find('>')
                tag = tagbrut[tb0 + 2:tb1 - 1]
                if debug: print("tag:", tag)
                # cursor.insertText("(AT "+tag+")")
                tags.append(tag)  # ajout du tag à la liste des tags appliqués ou à appliquer
                if debug: print ("les tags aprés ajout tag trouvé :",tags)
                # En fonction des tags retenus on applique le style
                setTags()
                data = data[t1 + 1:]
            if t0 > 0 or t0 == -1:
                if debug: print("l'élément suivant est un texte : ", data[:t0])
                if t0 > 0:
                    texte = data[:t0]
                    data = data[t0:]
                else:
                    texte = data
                    if debug: print("Texte restant : ", texte)
                    depart = False
                setTags()
                if len(tags) != 0:
                    if tags[-1] == "centre":
                        cursor.insertText(texte)
                    else:
                        cursor.insertText(texte, current_format)
                else:
                    cursor.insertText(texte)
            if t2 < t1 and t0 == 0:
                if debug: print("l'élement suivant est une fermeture de tag", data, " - Longueur restante : ", len(data))
                # Enlever le dernier tag du formatage
                setUnTags()
                tag = data[t0:t1 + 1]
                tags = tags[:-1]
                data = data[t1 + 1:]
                if len(data) == 12:
                    depart = False

        if i >= 1000: depart = False

        # Create a writer to save the document
        writer = QtGui.QTextDocumentWriter()
        writer.supportedDocumentFormats()
        odf_format = writer.supportedDocumentFormats()
        writer.setFormat(odf_format[1])
        writer.setFileName(file)
        writer.write(doc)  # Return True if successful
        if debug: print("FIN DE TRAITEMENT à ",i," boucles.")


def RepLancement():
    # en cas d'usage de PyInstaller pour créer un exe unique, il faut adapter le chemin du fichier ui,CSS
    if getattr(sys, 'frozen', False):
        wd = sys._MEIPASS
        print("bundle : path = ", wd,"plateforme :",platform.system())
        return wd
    else:
        wd = os.getcwd()
        print("live : path = ", wd,"plateforme :",platform.system())
        return wd

# Ouverture du fichier de configuration ou création de celui-ci
try:
    with open(newdatafile):
        # Lecture de la configuration dans le .ini
        config = cp.ConfigParser()
        config.read(newdatafile)
        defaulttheme = config.get('DefaultTheme', 'Default')
        defaultlettre = config.get('DefaultLetter', 'Default')
        defaultdir = config.get('DefaultDir', 'Default')
        base2=defaultdir+"\\"+base
        #print(config.sections())
        #print("Thème par défaut : ", defaulttheme)
        #print("Lettre par défaut : ", defaultlettre)
        for key in config['Themes']:
            #print("clé:", key, " - contenu :", config['Themes'][key])
            themes.append(config['Themes'][key])
        #print("Thèmes chargés :",themes)
        pass
except IOError:
    config = cp.ConfigParser()
    config.add_section('DefaultTheme')
    config.set('DefaultTheme', 'Default', 'Defaut')
    config.add_section('DefaultLetter')
    config.set('DefaultLetter','Default','A')
    config.add_section('DefaultDir')
    config.set('DefaultDir', 'Default', os.getcwd())
    defaulttheme='Default'
    config.add_section('Themes')
    config.set('Themes', 'Theme1', 'Defaut')
    config.set('Themes', 'Theme2', 'Python')
    themes.append('Defaut')
    themes.append('Python')
    with open(newdatafile, 'w') as settings:
        config.write(settings)
    print(themes)
    base2=os.getcwd()+"\\"+base
print(base2)

# Ouverture de la base de donnée ou création de celle-ci
try:
    with open(base2):
        conn = sqlite3.connect(base)
        cursor = conn.cursor()
        # cursor.execute("""INSERT INTO element(theme, lettre, data) VALUES(?, ?, ?)""", ("Defaut","A","lettre A"))
        conn.commit()
        conn.close()
        pass
except IOError:
    print ("Erreur de base de données! Le fichier n'a pas pu être ouvert")
    # Création ou connection à la base de donnée
    conn = sqlite3.connect(base2)
    # Création de la table
    cursor = conn.cursor()
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS element(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
     theme TEXT,lettre TEXT,data BLOB)
     """)
    conn.commit()
    conn.close()


def printlog(self,txt):
    self.log.insert_at_cursor(txt+"\n")


def trouveElementBase(self, theme,lettre):
    self.buffer.delete(self.buffer.get_start_iter(), self.buffer.get_end_iter())
    conn = sqlite3.connect(base2)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM element WHERE theme ='" + theme + "' AND lettre ='" + lettre + "'")
    try:
        resultat = list(cursor)[0][3]
        if debug: print(resultat)
        self.buffer.set_text('')
        format = self.buffer.register_deserialize_tagset()
        self.buffer.deserialize(self.buffer, format, self.buffer.get_end_iter(), resultat)
    except IndexError:
        printlog(self,"Pas encore d'entrée, création.")
        resultat = ""
        # TODO : Pour les nouveaux éléments taile text par defaut
        self.buffer.delete(self.buffer.get_start_iter(), self.buffer.get_end_iter())
        self.buffer.insert_at_cursor("Thème : "+theme+ " / Lettre "+lettre)
        self.buffer.apply_tag(self.tag_size12, self.buffer.get_start_iter(), self.buffer.get_end_iter())
    conn.close()
    printlog(self,"Lecture du thème {0}, lettre {1}".format(theme, lettre))
    self.label.set_text("Edition de la lettre "+lettre+" du thème "+theme+".")
    # Decoration du bouton
    for i,l in enumerate("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"):
        if lettre == l:
            print("déco du bouton",i)
            button[i].get_style_context().add_class("suggested-action")

def sauveElementBase(self, theme,lettre):
    conn = sqlite3.connect(base2)
    cursor = conn.cursor()

    # Sérialisation des données du buffer de TextView afin de les places dans la base SQL
    self.buffer.remove_source_marks(self.buffer.get_start_iter(), self.buffer.get_end_iter())
    data = self.buffer.get_text(self.buffer.get_start_iter(), self.buffer.get_end_iter(), include_hidden_chars=True)
    print("Sauvegarde du thème {0}, lettre {1}".format(theme, lettre))
    # print("Sauvegarde du thème {0}, lettre {1}, Data : {2}".format(theme, lettre, data[:40]))
    format = self.buffer.register_serialize_tagset()
    #print("Format : ",format)
    self.buffer.set_highlight_syntax(False)
    self.buffer.set_style_scheme(None)
    data = self.buffer.serialize(self.buffer, format, self.buffer.get_start_iter(), self.buffer.get_end_iter())
    print("Data sv sortie buffer :", data)
    if data.find(b'id=') !=-1:
        print("Attention tags de Sourceview")
        data2 = data.replace(b'<tags>\n  <tag id="0" priority="24">\n  </tag>\n </tags>', b'<tags>\n </tags>')
        if data.find(b'&lt;') !=-1 and data.find(b'&gt;') !=-1:
            print("symbole <...> trouvé")
            data3 = data2.replace(b'<apply_tag id="0">&lt;</apply_tag>', b'&lt;')
            data3 = data3.replace(b'<apply_tag id="0">&lt;', b'&lt;')
            data3 = data3.replace(b'<apply_tag id="0">&gt;</apply_tag>', b'&gt;')
            data2 = data3.replace(b'&gt;</apply_tag>',b'&gt;')
        if data.find(b'(') !=-1 and data.find(b')') !=-1:
            data3 = data2.replace(b'<apply_tag id="0">(</apply_tag>', b'(')
            data3 = data3.replace(b'<apply_tag id="0">(', b'(')
            data3 = data3.replace(b'<apply_tag id="0">)</apply_tag>', b')')
            data2 = data3.replace(b')</apply_tag>',b')')
        if data.find(b'[') !=-1 and data.find(b']') !=-1:
            data3 = data2.replace(b'<apply_tag id="0">[</apply_tag>', b'[')
            data3 = data3.replace(b'<apply_tag id="0">[', b'[')
            data3 = data3.replace(b'<apply_tag id="0">]</apply_tag>', b']')
            data2 = data3.replace(b']</apply_tag>',b']')
        if data.find(b'{') !=-1 and data.find(b'}') !=-1:
            data3 = data2.replace(b'<apply_tag id="0">{</apply_tag>', b'{')
            data3 = data3.replace(b'<apply_tag id="0">{', b'{')
            data3 = data3.replace(b'<apply_tag id="0">}</apply_tag>', b'}')
            data2 = data3.replace(b'}</apply_tag>',b'}')

        start_bytes = data2[0:26]
        size_bytes = data2[26:30]
        #print(start_bytes, "-", size_bytes)
        sizeval = int.from_bytes(size_bytes, byteorder='big', signed=False)
        #print(sizeval)
        end_of_markup = 29 + sizeval + 1
        #print(end_of_markup)
        the_rest = data2[end_of_markup:len(data2)]
        #print(the_rest)
        markup = data2[30:end_of_markup]
        #print(markup)
        newsize = len(markup).to_bytes(4, 'big')
        data = start_bytes + newsize + markup + the_rest
    print("Data sv modifié       :",data)
    cursor.execute("SELECT * FROM element WHERE theme ='" + theme + "' AND lettre ='" + lettre + "'")
    try:
        resultat = list(cursor)[0][3]
        #print(resultat)
        #cursor.execute("UPDATE element SET data='"+data+"' WHERE theme='"+theme+"' AND lettre='"+lettre+"'")
        cursor.execute("""UPDATE element SET data = ? WHERE theme = ? AND lettre = ?""", (data,theme,lettre))
    except IndexError:
        print("Erreur les données n'existent pas, création ...")
        cursor.execute("""INSERT INTO element(theme, lettre, data) VALUES(?, ?, ?)""", (theme, lettre, data))
        resultat = ""
    conn.commit()
    conn.close()
    return resultat

# Enlève la déco des tous les boutons lorsque l'on change de lettre
def removeDeco(self):
    for i in range(36):
        button[i].get_style_context().remove_class("suggested-action")

# Les trois fonctions suivantes enlèvent les tags du GtkTextView.
def removeColorTags(self,start,end):
    self.buffer.remove_tag(self.tag_black, start, end)
    self.buffer.remove_tag(self.tag_blue, start, end)
    self.buffer.remove_tag(self.tag_red, start, end)
    self.buffer.remove_tag(self.tag_green, start, end)
    self.buffer.remove_tag(self.tag_yellow, start, end)

def removeSizeTag(self,start,end):
    self.buffer.remove_tag(self.tag_size, start, end)
    self.buffer.remove_tag(self.tag_size10, start, end)
    self.buffer.remove_tag(self.tag_size11, start, end)
    self.buffer.remove_tag(self.tag_size12, start, end)
    self.buffer.remove_tag(self.tag_size13, start, end)
    self.buffer.remove_tag(self.tag_size14, start, end)
    self.buffer.remove_tag(self.tag_size15, start, end)

def removeTags(self,start,end):
    removeSizeTag(self,start,end)
    self.buffer.remove_tag(self.tag_bold, start, end)
    self.buffer.remove_tag(self.tag_italic, start, end)
    self.buffer.remove_tag(self.tag_underline, start, end)
    self.buffer.remove_tag(self.tag_center, start, end)
    self.buffer.remove_tag(self.tag_left, start, end)
    self.buffer.remove_tag(self.tag_right, start, end)
    self.buffer.remove_tag(self.tag_titre, start, end)
    self.buffer.remove_tag(self.tag_stitre1, start, end)
    self.buffer.remove_tag(self.tag_stitre2, start, end)
    self.buffer.remove_tag(self.tag_strikethrough, start, end)

def removeAlignTags(self,start,end):
    self.buffer.remove_tag(self.tag_center, start, end)
    self.buffer.remove_tag(self.tag_left, start, end)
    self.buffer.remove_tag(self.tag_right, start, end)

# Fonction pour les boutons A à Z et 0 à 9 - l'appel vient des callback des boutons
def ActionAlphaButtons(self,lettre):
    global defaulttheme, defaultlettre
    print("Bouton '"+lettre+"'")
    # Mise a jour de la déco des boutons
    removeDeco(self)
    for i, l in enumerate("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"):
        if lettre == l:
            button[i].get_style_context().add_class("suggested-action")
    # Sauve l'élément courant et désigne le nouvel élément avant de le charger
    sauveElementBase(self, defaulttheme, defaultlettre)
    defaultlettre = lettre
    trouveElementBase(self, defaulttheme, defaultlettre)
#####################################
# Définition de la classe 'Interface'
#####################################
class Interface:
    __gtype_name__ = "CarnetAlphabétiqueThématique"

    # initialisateur de la méthode 'Interface' (état de départ)
    def __init__(self):
        global wd,defaulttheme,defaultlettre,defaultdir
        wd = RepLancement()
        # Création et chargement de l'interface
        interface = Gtk.Builder()
        interface.add_from_file('Interface.glade')
        # Connecter les signaux
        interface.connect_signals(self)
        # variables
        self.window = interface.get_object("window")
        self.window.set_icon_from_file("logo4.png")
        self.set_style()
        self.setup = interface.get_object("Dialog1")
        self.aide = interface.get_object("Dialog2")
        self.label = interface.get_object('labelremarque')
        self.repch = interface.get_object('filechooserbutton1')

        # Définition des boutons A......0...9
        for i in "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789":
            button.append(interface.get_object('button'+i))

        # Objets de la fenêtre principale
        # les logs
        self.textviewlog = interface.get_object('textviewlog')
        self.scrolledwindow = interface.get_object('scrolledwindowlog')
        self.log = self.textviewlog.get_buffer()
        # zone de texte principale
        self.buffer = GtkSource.Buffer()
        self.buffer.set_highlight_syntax(False)
        self.buffer.set_style_scheme(None)
        #stylescheme = self.buffer.get_style_scheme()
        #print(stylescheme)
        self.buffer.set_language()
        self.buffer.set_highlight_syntax(False)
        self.textview = GtkSource.View(buffer=self.buffer)
        self.textview.set_editable(True)
        self.textview.set_cursor_visible(True)
        self.textview.set_show_line_numbers(False)
        #fontdesc = Pango.FontDescription("Monospace 12")
        #self.textview.override_font(fontdesc)
        self.scrolledwindowNote = interface.get_object('scrolledwindowNote')
        self.scrolledwindowNote.add(self.textview)
        self.textview.connect("button-release-event",self.selection)
        self.buffer.connect("changed",self.selection)
        # Dialogue d'aide
        self.textviewaide=interface.get_object('textviewaide')
        self.bufferaide=self.textviewaide.get_buffer()
        self.tag_bold = self.buffer.create_tag("bold", weight=Pango.Weight.BOLD)
        self.tag_italic = self.buffer.create_tag("italic", style=Pango.Style.ITALIC)
        self.tag_underline = self.buffer.create_tag("underline", underline=Pango.Underline.SINGLE)
        self.tag_strikethrough = self.buffer.create_tag("strikethrough", strikethrough=True)
        self.tag_center = self.buffer.create_tag("centre", justification=Gtk.Justification.CENTER)
        self.tag_left = self.buffer.create_tag("left", justification=Gtk.Justification.LEFT)
        self.tag_right = self.buffer.create_tag("right", justification=Gtk.Justification.RIGHT)
        self.tag_found = self.buffer.create_tag("found", background="yellow")
        self.tag_size = self.buffer.create_tag("size", size_points=10)
        self.tag_size10 = self.buffer.create_tag("size10", size_points=10)
        self.tag_size11 = self.buffer.create_tag("size11", size_points=11)
        self.tag_size12 = self.buffer.create_tag("size12", size_points=12)
        self.tag0 = self.buffer.create_tag("tag0", size_points=12)
        self.tag_size13 = self.buffer.create_tag("size13", size_points=13)
        self.tag_size14 = self.buffer.create_tag("size14", size_points=14)
        self.tag_size15 = self.buffer.create_tag("size15", size_points=15)
        self.tag_titre = self.buffer.create_tag("titre", weight=Pango.Weight.BOLD, underline=Pango.Underline.SINGLE, size_points=15)
        self.tag_stitre1 = self.buffer.create_tag("stitre1", weight=Pango.Weight.BOLD, underline=Pango.Underline.SINGLE, size_points=12)
        self.tag_stitre2 = self.buffer.create_tag("stitre2", weight=Pango.Weight.BOLD, size_points=12)

        self.tag_black = self.buffer.create_tag("black", foreground="black")
        self.tag_blue = self.buffer.create_tag("blue", foreground="blue")
        self.tag_red = self.buffer.create_tag("red", foreground="red")
        self.tag_green = self.buffer.create_tag("green", foreground="green")
        self.tag_yellow = self.buffer.create_tag("yellow", foreground="yellow")

        printlog(self, "Backup de la base de données")
        date = str(datetime.datetime.now())
        date = date.replace(":", "-")
        shutil.copy("TEC Base.db", "TEC Base " + str(date) + ".db")
        # print ("init : DT {0} - DL {1}".format(defaulttheme,defaultlettre))
        # Chargement initial de l'élément pas défaut
        # print("Intreface, Thème par défaut : ", defaulttheme)
        #print("Intreface, Lettre par défaut : ", defaultlettre)
        removeDeco(self)
        trouveElementBase(self, defaulttheme, defaultlettre)

        # combo de la fenêtre principale
        self.combostyle = interface.get_object('comboboxtextstyle')
        self.combo= interface.get_object('comboboxtexttheme')
        self.combo.remove_all()
        for i,elt in enumerate(themes):
            self.combo.append_text(elt)
            if elt==defaulttheme:
                self.combo.set_active(i)
        print("init : DT {0} - DL {1}".format(defaulttheme, defaultlettre))
        # Objets des dialogues
        self.nom = interface.get_object('entryDialogNom')
        # Objets du dialogue : TreeView
        self.treeview = interface.get_object('treeview')
        renderer=Gtk.CellRendererText()
        column=Gtk.TreeViewColumn("Liste des thèmes",renderer,text=0)
        self.treeview.append_column(column)
        self.list_store1= Gtk.ListStore(str)
        self.list_store1.append(["test1"])
        self.list_store1.append(["test2"])
        self.treeview.set_model(self.list_store1)

        # montrer l'interface
        self.window.show_all()

##############################################################################
    def set_style(self):
        """ Change Gtk+ Style """
        provider = Gtk.CssProvider()
        # Demo CSS kindly provided by Numix project
        provider.load_from_path(join(wd, 'MyCSS.css'))
        screen = Gdk.Display.get_default_screen(Gdk.Display.get_default())
        # I was unable to found instrospected version of this
        GTK_STYLE_PROVIDER_PRIORITY_APPLICATION = 600
        Gtk.StyleContext.add_provider_for_screen(
            screen, provider,
            GTK_STYLE_PROVIDER_PRIORITY_APPLICATION
        )
    # Astuce permet le defilement automatique du treeviw ds la scrolledwindow
    # GtkTextview - Signals - GtkWidget - size-allocate
    def treeview_changed(self, widget, event, data=None):
        adj = self.scrolledwindow.get_vadjustment()
        adj.set_value(adj.get_upper() - adj.get_page_size())

    def onButtonSetup(self,widget):
        global defaultdir
        self.setup.show_all()
        # Effacer le GtkTreeView (list_store) & chargement de la liste des thèmes
        self.list_store1.clear()
        log="Liste des thèmes : "
        for line in themes:
            log=log+line+" - "
            self.list_store1.append([line])
        printlog(self, log[:len(log)-3])
        reponse = self.setup.run()
        print(reponse)
        if reponse == 0: self.setup.hide() # Sortie avec Quit
        if reponse == -4: self.setup.hide()  # Sortie avec la croix
        if reponse == 1: # Sortie avec ok
            self.setup.hide()
            print("Enregistrement des thèmes")
            config = cp.ConfigParser()
            config.read(newdatafile)
            # On efface la liste des themes pour la recréer
            config.remove_section('Themes')
            config.add_section('Themes')
            for i,theme in enumerate(themes):
                config.set('Themes', 'Theme'+str(i), theme)
            # Mise à jour du .ini
            with open(newdatafile, 'w') as settings:
                config.write(settings)
            # Mise a jour du combobox
            self.combo.remove_all()
            log = "Liste des thèmes : "
            for i, elt in enumerate(themes):
                log = log + elt + " - "
                self.combo.append_text(elt)
                if elt == defaulttheme:
                    self.combo.set_active(i)
            printlog(self, log[:len(log) - 3])

    def ontogglebutton(self,widget):
        global togglebuttonstate
        if widget.get_active():
            togglebuttonstate=True
            print("activé")
        else:
            togglebuttonstate=False
            print("inactif")

    def ontogglebuttondebug(self,widget):
        global debug
        if widget.get_active():
            debug=True
            print("activé")
        else:
            debug=False
            print("inactif")

    def onbuttonprint(self,widget):
        printlog(self,"Impression de la note...")
        format = self.buffer.register_serialize_tagset("application/x-gtk-text-buffer-rich-text")
        data = self.buffer.serialize(self.buffer, format, self.buffer.get_start_iter(), self.buffer.get_end_iter())
        if debug: print("[1] -> ",data)
        data = data.decode(errors='ignore')
        data=str(data).split('<text_view_markup>', 1)
        del data[0]
        data[0] = '<text_view_markup>' + str(data[0])
        data = data[0].split('</tags>', 1)
        del data[0]
        data = data[0].split('</text_view_markup>', 1)
        data = str(data[0]).replace("\n", '<br>')
        data=data.replace("&apos;","'")
        data=data.replace("&quot;",'"')
        data=data.replace("&amp;","&")
        data=data.replace("&gt;",">")
        data=data.replace("&lt;",">")
        if debug: print("[2] -> ", data)

        SaveFileInOdt(data, defaulttheme+" - "+defaultlettre+".odt")

    def onDirChooser(self,widget):
        global defaultdir
        #self.dirInput = widget.get_filename()
        print("Repertoire choisi: ")
        dialog = Gtk.FileChooserDialog(title="Choisir un répertoire", parent=None,
                                       action=Gtk.FileChooserAction.SELECT_FOLDER,)
        dialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,"Select", Gtk.ResponseType.OK)
        dialog.set_default_size(800, 400)
        print(defaultdir)
        dialog.set_current_folder(defaultdir)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("Select clicked")
            olddir=defaultdir
            defaultdir= dialog.get_filename()
            print("Folder selected: " + defaultdir)
            printlog(self,"Changement du repertoire de la base")
            config = cp.ConfigParser()
            config.read(newdatafile)
            # On efface la liste des themes pour la recréer
            config.remove_section('DefaultDir')
            config.add_section('DefaultDir')
            config.set('DefaultDir', 'default', defaultdir)
            # Mise à jour du .ini
            with open(newdatafile, 'w') as settings:
                config.write(settings)
            # Déplacement de la base et du fichier de configuration vers le nouveau répertoire
            if defaultdir != olddir and togglebuttonstate==True:
                shutil.copyfile(olddir+"\\"+base, defaultdir+"\\"+base)
                shutil.copyfile(olddir + "\\"+ini, defaultdir + "\\"+ini)
                dialog2 = Gtk.MessageDialog(parent=Gtk.Window(), flags=0, message_type=Gtk.MessageType.INFO,
                                            buttons=Gtk.ButtonsType.OK, text="Fichier de base de données déplacé.")
                dialog2.format_secondary_text("Il faut quitter et redémarrer.")
                dialog2.run()
                dialog2.destroy()
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()

    def OnButtonDialogNettoyer(self, widget):
        printlog(self, "Nettoyage base.")
        global themes
        conn = sqlite3.connect(base)
        cursor = conn.cursor()
        sql='SELECT * FROM element WHERE theme NOT IN ({})'.format(','.join('?' * len(themes)))
        row= cursor.execute(sql, themes).fetchone()
        # Recherche de la liste des thèmes à effacer
        deletedThemes=[]
        while row != None:
            rep=0
            for testth in deletedThemes:
                if row[1] == testth:
                    rep=1 # Déjà dans la liste
            if rep==0:
                # Le thème à effacer n'est pas déjà dans la liste, on l'ajoute
                deletedThemes.append(row[1])
            row = cursor.fetchone() # Entrée suivante qui contient des données à effacer
        print("Thèmes à détruire : ",deletedThemes)
        if deletedThemes !="":
            for th in deletedThemes:
                printlog(self,"Effacement des entrées du thème : {0}".format(th))
                cursor.execute("DELETE FROM element WHERE theme = '"+th+"'")
        conn.commit()
        conn.close()

    def OnButtonDialogSup(self, widget):
        global setupthemeselect
        print("dialog,sup")
        # Le thème sélectionné ne doit pas être celui actif
        if setupthemeselect == defaulttheme:
            dialog1 = Gtk.MessageDialog(parent=Gtk.Window(), flags=0, message_type=Gtk.MessageType.INFO,
                                        buttons=Gtk.ButtonsType.OK, text="Le thème à supprimer est celui actif !")
            dialog1.format_secondary_text("Activer un autre thème pour supprimer !.")
            dialog1.run()
            dialog1.destroy()
        else:
            if setupthemeselect == "":
                dialog1 = Gtk.MessageDialog(parent=Gtk.Window(), flags=0, message_type=Gtk.MessageType.INFO,
                                            buttons=Gtk.ButtonsType.OK, text="pas de thème sélectionné !")
                dialog1.run()
                dialog1.destroy()
            for i,suptheme in enumerate(themes):
                if suptheme == setupthemeselect:
                    printlog(self,"thème à supprimer trouvé dans la liste")
                    del themes[i]
            # Effacer le GtkTreeView (list_store) et le remettre à jour
            self.list_store1.clear()
            for line in themes:
                self.list_store1.append([line])


    def onTreeSelectionChanged(self, selection):
        global setupthemeselect
        model, treeiter = selection.get_selected()
        if treeiter is not None:
            print("You selected", model[treeiter][0])
            setupthemeselect=model[treeiter][0]

    def OnButtonDialogCreer(self, widget):
        print("dialog, créer")
        nom = self.nom.get_text()
        # S'assurer que le nom n'existe pas deja
        error=False
        for enom in themes:
            if nom == enom:
                dialog1 = Gtk.MessageDialog(parent=Gtk.Window(), flags=0, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK, text="Le thème existe déjà !")
                dialog1.format_secondary_text("Changer de nom !.")
                dialog1.run()
                dialog1.destroy()
                error=True
        if nom != "" and error is False:
            themes.append(nom)
            self.list_store1.append([nom])
        print(themes)

    # lien avec glade : comboboxtexttheme / signaux / GtkComboBox / changed -> comboboxtexttheme_changed
    def comboboxtexttheme_changed(self, widget, data=None):
        global defaulttheme, defaultlettre,callback
        model = widget.get_model()
        active = widget.get_active()
        print("ComboThème")
        if active >= 0:
            if  callback == True:
                temptheme = model[active][0]
                print("Thème : ", temptheme, "n°", active)
                # Sauvegarde de l'élément courant
                sauveElementBase(self,defaulttheme, defaultlettre)
                # Changement du thème par defaut
                defaulttheme=temptheme
                # Chargement de l'élément, lettre "A" imposé
                # defaultlettre = "A"
                trouveElementBase(self, defaulttheme, defaultlettre)
            else:
                callback = True
        else:
            print("erreur combo")

    def comboboxstyle_changed(self,widget,data=None):
        model = widget.get_model()
        active = widget.get_active()
        if active >= 0:
            style = model[active][0]
            print("Ch taille :", style)
            bounds = self.buffer.get_selection_bounds()
            if len(bounds) != 0:
                start, end = bounds
                if style =="Standart":
                    self.buffer.remove_all_tags(start, end)
                if style =="Titre":
                    removeTags(self, start, end)
                    self.buffer.apply_tag(self.tag_titre, start, end)
                if style == "Sous Titre 1":
                    removeTags(self, start, end)
                    self.buffer.apply_tag(self.tag_stitre1, start, end)
                if style == "Sous Titre 2":
                    removeTags(self, start, end)
                    self.buffer.apply_tag(self.tag_stitre2, start, end)

    def selection(self,widget,data=None):
        # print("Sélection de texte")
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("selection")
            # print("start :",start.begins_tag(self.tag_titre),"-",start.ends_tag(self.tag_titre),"-",start.toggles_tag(self.tag_titre),"-",start.has_tag(self.tag_titre))
            # print("end :", end.begins_tag(self.tag_titre), "-", end.ends_tag(self.tag_titre), "-", end.toggles_tag(self.tag_titre), "-", end.has_tag(self.tag_titre))
            print(start.has_tag(self.tag_titre),"-",end.has_tag(self.tag_titre))
            if start.toggles_tag(self.tag_titre) and end.toggles_tag(self.tag_titre):
                print("vrai titre")
                self.combostyle.set_active(0)
            if start.toggles_tag(self.tag_stitre1) and end.toggles_tag(self.tag_stitre1):
                print("vrai sous titre 1")
                self.combostyle.set_active(1)
            if start.toggles_tag(self.tag_stitre2) and end.toggles_tag(self.tag_stitre2):
                print("vrai sous titre 2")
                self.combostyle.set_active(2)
            if not start.toggles_tag(self.tag_titre) and not end.toggles_tag(self.tag_titre) and not start.toggles_tag(self.tag_stitre1) and not end.toggles_tag(self.tag_stitre1) and not start.toggles_tag(self.tag_stitre2) and not end.toggles_tag(self.tag_stitre2):
                print("vrai aucun style")
                self.combostyle.set_active(3)

    def comboboxtextpoints_changed(self,widget,data=None):
        model = widget.get_model()
        active = widget.get_active()
        if active >= 0:
            size = model[active][0]
            print("Ch taille :",size)
            bounds = self.buffer.get_selection_bounds()
            if len(bounds) != 0:
                start, end = bounds
                if size == "10":
                    removeSizeTag(self,start,end)
                    self.buffer.apply_tag(self.tag_size10, start, end)
                if size == "11":
                    removeSizeTag(self,start,end)
                    self.buffer.apply_tag(self.tag_size11, start, end)
                if size == "12":
                    removeSizeTag(self, start, end)
                    self.buffer.apply_tag(self.tag_size12, start, end)
                if size == "13":
                    removeSizeTag(self, start, end)
                    self.buffer.apply_tag(self.tag_size13, start, end)
                if size == "14":
                    removeSizeTag(self, start, end)
                    self.buffer.apply_tag(self.tag_size14, start, end)
                if size == "15":
                    removeSizeTag(self, start, end)
                    self.buffer.apply_tag(self.tag_size15, start, end)

    def on_button_aide_clicked(self,widget):
        self.aide.show_all()
        self.bufferaide.delete(self.bufferaide.get_start_iter(), self.bufferaide.get_end_iter())
        self.bufferaide.insert_at_cursor("L'interface se décompose en plusieurs zones:\n\n")
        self.bufferaide.insert_at_cursor(" - A gauche le choix de la lettte correspondant à la note, de A & Z et 0 à 9,\n")
        self.bufferaide.insert_at_cursor(" - En haut, de gauche à droite :\n")
        self.bufferaide.insert_at_cursor("      - le choix du thème du carnet actif,\n")
        self.bufferaide.insert_at_cursor("      - le style de texte,\n")
        self.bufferaide.insert_at_cursor("      - undo et redo, ")
        self.bufferaide.insert_markup(self.bufferaide.get_end_iter(), "<b>attention ça ne marche que sur le texte pas sur la mise en forme,</b>\n", -1)
        self.bufferaide.insert_at_cursor("      - une suite d'outils pour mettre en gras, souligné, ...\n")
        self.bufferaide.insert_at_cursor("      - le choix de la taille de caractères,\n")
        self.bufferaide.insert_at_cursor("      - 5 icônes pour changer la couleur du texte.\n")
        self.bufferaide.insert_at_cursor(" - En bas, 2 icones, un pour les réglages et un autre pour afficher cette aide,\n")
        self.bufferaide.insert_at_cursor(" - Enfin tout en bas de la fenêtre, une zone de texte de log :\n\n")
        self.bufferaide.insert_at_cursor(" - Setup / Réglages :\n\n")
        self.bufferaide.insert_at_cursor("      - possibilité de créer, supprimer des thèmes,\n")
        self.bufferaide.insert_at_cursor("      - possibilité de renommer un thème (pas encore implémenté),\n")
        self.bufferaide.insert_at_cursor("      - nettoyage de la base si un thème a été supprimé.\n")
        self.bufferaide.insert_at_cursor("      - déplacement de la base de données vers un nouvel emplacement :\n\n")
        self.bufferaide.insert_markup(self.bufferaide.get_end_iter(), "          - <b>dans tous les cas le fichier .ini doit rester dans le répertoire de l'éxécutable</b>\n",-1)
        self.bufferaide.insert_markup(self.bufferaide.get_end_iter(), "          - <b>si le checkbox 'Copier le fichier existant' est activé les fichier .db et .ini sont copiés au nouvel emplacement: </b>\n",-1)
        self.bufferaide.insert_at_cursor("              - activer le checkbox pour déplacer la base,\n")
        self.bufferaide.insert_at_cursor("              - ne pas activer le checkbox si la base à utiliser est déjà dans le répertoire choisis (migration, cloud),\n")
        self.bufferaide.insert_at_cursor("              - le fichier .ini peut être recopié dans le dosseir de l'application (editer le et modifier le chemin de l'exe),\n")
        self.bufferaide.insert_markup(self.bufferaide.get_end_iter(),"              - <b>REDEMARRER L'APPLICATION pour que la nouvelle base sont activée.</b>\n",-1)
        reponse = self.aide.run()
        print(reponse)
        if reponse == 0: self.aide.hide()  # Sortie avec Quit
        if reponse == -4: self.aide.hide()  # Sortie avec la croix
        if reponse == 1: self.aide.hide() # Sortie avec ok

    def on_button_undo(self,widget):
        print("undo")
        if self.buffer.can_undo():
            self.buffer.do_undo(self.buffer)
    def on_button_redo(self, widget):
        print("Redo")
        if self.buffer.can_redo():
            self.buffer.do_redo(self.buffer)

    def on_button_blue_clicked(self,widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeColorTags(self,start,end)
            self.buffer.apply_tag(self.tag_blue, start, end)

    def on_button_red_clicked(self,widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeColorTags(self,start,end)
            self.buffer.apply_tag(self.tag_red, start, end)

    def on_button_green_clicked(self,widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeColorTags(self,start,end)
            self.buffer.apply_tag(self.tag_green, start, end)

    def on_button_yellow_clicked(self,widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeColorTags(self,start,end)
            self.buffer.apply_tag(self.tag_yellow, start, end)

    def on_button_black_clicked(self,widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeColorTags(self,start,end)
            self.buffer.apply_tag(self.tag_black, start, end)

    def on_button_bold_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            self.buffer.apply_tag(self.tag_bold,start,end)

    def on_button_italic_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            self.buffer.apply_tag(self.tag_italic,start,end)

    def on_button_underline_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            self.buffer.apply_tag(self.tag_underline,start,end)

    def on_button_strikethrough_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            self.buffer.apply_tag(self.tag_strikethrough,start,end)

    def on_button_gauche_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeAlignTags(self,start,end)
            self.buffer.apply_tag(self.tag_left,start,end)

    def on_button_droite_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeAlignTags(self,start,end)
            self.buffer.apply_tag(self.tag_right,start,end)

    def on_button_centre_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeAlignTags(self,start,end)
            self.buffer.apply_tag(self.tag_center,start,end)

    # Enlever tous les tags d'une zone.
    def on_button_effacer_clicked(self, widget):
        bounds = self.buffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            print("Click sur bouton de style : {0} - {1}".format(start, end))
            removeTags(self, start, end)
            removeAlignTags(self, start, end)
            removeColorTags(self, start, end)

    # Liste de tous les callback des boutons A à Z et 0 à 9 - Chaque callback appel ActionAlphaButton()
    # lien avec glade pour tous les boutons : Gtkbutton / Signals / GtkButton / clicked ->onButton...Clicked
    def onButtonAClicked(self, widget):
        ActionAlphaButtons(self,"A")
    def onButtonBClicked(self, widget):
        ActionAlphaButtons(self,"B")
    def onButtonCClicked(self, widget):
        ActionAlphaButtons(self,"C")
    def onButtonDClicked(self, widget):
        ActionAlphaButtons(self,"D")
    def onButtonEClicked(self, widget):
        ActionAlphaButtons(self,"E")
    def onButtonFClicked(self, widget):
        ActionAlphaButtons(self,"F")
    def onButtonGClicked(self, widget):
        ActionAlphaButtons(self,"G")
    def onButtonHClicked(self, widget):
        ActionAlphaButtons(self,"H")
    def onButtonIClicked(self, widget):
        ActionAlphaButtons(self,"I")
    def onButtonJClicked(self, widget):
        ActionAlphaButtons(self,"J")
    def onButtonKClicked(self, widget):
        ActionAlphaButtons(self,"K")
    def onButtonLClicked(self, widget):
        ActionAlphaButtons(self,"L")
    def onButtonMClicked(self, widget):
        ActionAlphaButtons(self,"M")
    def onButtonNClicked(self, widget):
        ActionAlphaButtons(self,"N")
    def onButtonOClicked(self, widget):
        ActionAlphaButtons(self,"O")
    def onButtonPClicked(self, widget):
        ActionAlphaButtons(self,"P")
    def onButtonQClicked(self, widget):
        ActionAlphaButtons(self,"Q")
    def onButtonRClicked(self, widget):
        ActionAlphaButtons(self,"R")
    def onButtonSClicked(self, widget):
        ActionAlphaButtons(self,"S")
    def onButtonTClicked(self, widget):
        ActionAlphaButtons(self,"T")
    def onButtonUClicked(self, widget):
        ActionAlphaButtons(self,"U")
    def onButtonVClicked(self, widget):
        ActionAlphaButtons(self,"V")
    def onButtonWClicked(self, widget):
        ActionAlphaButtons(self,"W")
    def onButtonXClicked(self, widget):
        ActionAlphaButtons(self,"X")
    def onButtonYClicked(self, widget):
        ActionAlphaButtons(self,"y")
    def onButtonZClicked(self, widget):
        ActionAlphaButtons(self,"Z")
    def onButton0Clicked(self, widget):
        ActionAlphaButtons(self,"0")
    def onButton1Clicked(self, widget):
        ActionAlphaButtons(self,"1")
    def onButton2Clicked(self, widget):
        ActionAlphaButtons(self,"2")
    def onButton3Clicked(self, widget):
        ActionAlphaButtons(self,"3")
    def onButton4Clicked(self, widget):
        ActionAlphaButtons(self,"4")
    def onButton5Clicked(self, widget):
        ActionAlphaButtons(self,"5")
    def onButton6Clicked(self, widget):
        ActionAlphaButtons(self,"6")
    def onButton7Clicked(self, widget):
        ActionAlphaButtons(self,"7")
    def onButton8Clicked(self, widget):
        ActionAlphaButtons(self,"8")
    def onButton9Clicked(self, widget):
        ActionAlphaButtons(self,"9")

    # Lien avec glade : windows / Signals / GtkWidget / destroy -> on_destroy
    def on_destroy(self, widget):
        # Sauve l'élément courant dans la base
        print("Appel sauvegarde fin")
        sauveElementBase(self, defaulttheme, defaultlettre)
        # Sauve la configuration
        config = cp.ConfigParser()
        config.read(newdatafile)
        # Modification des thèmes et lettre par défaut
        config.set('DefaultTheme', 'Default', defaulttheme)
        config.set('DefaultLetter', 'Default', defaultlettre)
        # Mise à jour du .ini
        with open(newdatafile, 'w') as settings:
            config.write(settings)
        print("Au Revoir !")
        Gtk.main_quit()

# exécute le module sauf s'il est importé
if __name__ == "__main__":
    # Création de l'interface graphique
    app = Interface()
    # Boucle principale
    Gtk.main()

